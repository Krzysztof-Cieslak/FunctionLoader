module FunctionLoader

open System.IO

type Transformation = string -> string

module Register =
    type private Msg =
        | Add of string * Transformation
        | Remove of string
        | Get of AsyncReplyChannel<Transformation list>

    let private register =
        MailboxProcessor.Start (fun inbox ->
            let rec loop lst = async {
                let! msg = inbox.Receive()
                match msg with
                | Add (n,f) ->
                    return! loop ((n,f)::lst)
                | Remove n ->
                    return! loop (lst |> List.filter(fun (f,_) -> f <> n))
                | Get rc ->
                    let l = lst |> List.map snd
                    rc.Reply l
                    return! loop lst
            }
            loop [] )

    let add name fnc =
        (name, fnc) |> Add |> register.Post

    let remove name =
        name |> Remove |> register.Post

    let get () =
        register.PostAndReply Get

module Evaluator =
    open System.Globalization
    open System.Text
    open Microsoft.FSharp.Compiler.Interactive.Shell

    let sbOut = StringBuilder()
    let sbErr = StringBuilder()

    let fsi =
        let inStream = new StringReader("")
        let outStream = new StringWriter(sbOut)
        let errStream = new StringWriter(sbErr)
        try
            let fsiConfig = FsiEvaluationSession.GetDefaultConfiguration()
            let argv = [| "/temo/fsi.exe"; |]
            FsiEvaluationSession.Create(fsiConfig, argv, inStream, outStream, errStream)
        with
        | ex ->
            printfn "Error: %A" ex
            printfn "Inner: %A" ex.InnerException
            printfn "ErrorStream: %s" (errStream.ToString())

            raise ex

    let getOpen path =
        let path = Path.GetFullPath path
        let filename = Path.GetFileNameWithoutExtension path
        let textInfo = (CultureInfo("en-US", false)).TextInfo
        textInfo.ToTitleCase filename

    let getLoad path =
         let path = Path.GetFullPath path
         path.Replace("\\", "\\\\")



    let evaluate path =

        let filename = getOpen path
        let load = getLoad path

        let _, errs = fsi.EvalInteractionNonThrowing(sprintf "#load \"%s\";;" load)
        if errs.Length > 0 then printfn "Load Erros : %A" errs

        let _, errs = fsi.EvalInteractionNonThrowing(sprintf "open %s;;" filename)
        if errs.Length > 0 then printfn "Open Erros : %A" errs

        let res,errs = fsi.EvalExpressionNonThrowing "map"
        if errs.Length > 0 then printfn "Get map Errors : %A" errs

        match res with
        | Choice1Of2 (Some f) ->
            f.ReflectionValue :?> Transformation |> Some
        | _ -> None

module Watcher =


    let create addCb rmCb dir =
        if Directory.Exists dir |> not then Directory.CreateDirectory dir |> ignore

        let watcher = new FileSystemWatcher()
        watcher.Filter <- "*.fsx"
        watcher.Path <- dir
        watcher.Created.Add (fun n -> n.FullPath |> addCb)
        watcher.Deleted.Add (fun n -> n.FullPath |> rmCb)
        watcher.Renamed.Add (fun n -> n.OldFullPath |> rmCb; n.FullPath |> addCb)
        watcher.Changed.Add (fun n -> n.FullPath |> rmCb; n.FullPath |> addCb)
        watcher.SynchronizingObject <- null
        watcher.EnableRaisingEvents <- true

        watcher



[<EntryPoint>]
let main argv =
    let remove path =
        let fn = Path.GetFileNameWithoutExtension path
        Register.remove fn

    let add path =
        let fn = Path.GetFileNameWithoutExtension path
        match Evaluator.evaluate path |> Option.map (fun ev -> Register.add fn ev ) with
        | Some _ -> ()
        | None -> printfn "File `%s` couldn't be parsed" path

    let watcher = Watcher.create add remove "scripts"

    while true do
        let input = System.Console.ReadLine ()
        let lst = Register.get ()
        let res = lst |> List.fold (fun s e -> e s ) input
        printfn "Result: %s" res

    0 // return an integer exit code
